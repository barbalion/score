# One line comment
# Multiline1
	Comment
# Multiline2
	nested
		comment
	continued

#One-line 'is' declarations
Asset

Asset is Object

Asset is an Object

Asset is a Object

Assets are Objects

Asset (Object)

Asset is Object # comment

Asset is Object with # comment1
	# comment2
	Var1 1 # comment3

# One-line with vars
Asset is Object with Var 1
Asset is Object with Var 1, Var 2
Asset is Object with Var 1, Var 2 # comment
Object for Asset with Var 1, Var 2 # comment

Asset (Object) with Var 1, Var 2


#Multi-line 'is' declarations with many extra spaces
Asset is Object with
	Var1 1

Asset   is   Object   with  
	Var1    1  
	Var2   2  

# Modifiers
Asset!
Asset?

Asset? is Object
Asset! is Object

# Expressions
A(B) with C (1+1/2/3*3*3**4<>Ggg)

# 'has' declarations
A has B
A has B (C) with Var1 true

B in A
B in A with Var1 true

A has
	B
	C (B) with Var1 "str"
	D has
		E (F) with
			Name "
				Multiline3

# when
A is B when true
A is B whenNot false
A has B when true
A has
	B (C) with
		Var1 D
		when true or true or true or true
A whenNot B = C

# all together
Asset (Object) with
	EmptyStrVar ""
	Var1 "Oneline string"
	Var2 "
		Multiline4
			String # comment-part of string
				Two Level indented string
		Quote " a part of string
	Var3 1 #"Oneline string"
	Var4 "String with value $(Name) inlined "
	when P + 1 /2 **2 *4 // 3 % H = 2 or 1 >3 xor 2<=6 and T != G or (1+2 <> 44e2 + 1e-5 - 100_000 % -3 and 5>=7 or not 6<2)
