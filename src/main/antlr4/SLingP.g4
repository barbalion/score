parser grammar SLingP; // Score Data Flow Language

options {
    tokenVocab=SLingL;
}

@members {
	private static final int IS_BODY_BLOCK = 1;
}

// Parser
parse: NEWLINE* clause* EOF;

// Root clauses
clause: (is_decl | has_decl | comment | alias_decl) eol*;

// 'is' declaration
is_decl: a_single_decl | aisb_decl | bfora_decl | abracketb_decl;
a_single_decl: new_name (is_body | eol);
aisb_decl: new_name is known_names is_body;
bfora_decl: known_name FOR new_name is_body;
abracketb_decl: new_name OPEN_PAREN known_names CLOSE_PAREN is_body;

is_body: is_body_oneline | is_body_mline;
is_body_oneline: (WITH with_vars_oneline)? whenOrNot? eol ;
is_body_mline: WITH eol INC_INDENT with_varline+ (whenOrNot eol)? DEC_INDENT ;
when: WHEN expr_bool;
whenNot: WHENNOT expr_bool;
whenOrNot: (when | whenNot);

with_vars_oneline: with_var (COMMA with_vars_oneline)?;
with_var: (known_name value) | (value as known_name) ;
with_varline: (with_var eol | with_var_str | comment) ;
with_var_str : known_name string_mline ;

// 'has' declaration
has_decl: ahasb_decl | bina_decl;
ahasb_decl: known_name has has_body;
bina_decl: new_name (OPEN_PAREN known_names CLOSE_PAREN)? in known_name is_body;
has_body: is_decl | has_mbody;
has_mbody: eol INC_INDENT clause* DEC_INDENT;

// alias
alias_decl: known_name as name;

name : ID ;
as : AS ;
has: HAS | HAVE ;
in: IN;
of: OF;

value: number | string | bool_literal | known_name | expr;

expr: OPEN_PAREN (expr_bool | expr_calc | str_expr) CLOSE_PAREN;

eol: comment_on_eol? NEWLINE;
new_name: name | name EXCLAMARK | name QUESTMARK;
known_name: name | known_name name | name of known_name;
known_names: known_name (COMMA known_names)?;

is: (IS | IS A | IS AN | ARE) ;
//in: IN ;
//has: SPACES (HAS | HAVE) SPACES ;

comment: COMMENT_START COMMENT_TEXT? NEWLINE (INC_INDENT comment_mline+ DEC_INDENT)?;
comment_on_eol : COMMENT_ON_EOL;
comment_mline: COMMENT_TEXT? NEWLINE | (INC_INDENT comment_mline+ DEC_INDENT);

str_expr: string | str_concat;
string: string_oneline | string_mline;
string_oneline: STRING_ONELINE_START string_oneline_content_parsed? STRING_ONELINE_CLOSE;
string_oneline_content_parsed : (STRING_ONELINE_CONTENT | STRING_ONELINE_EXPR known_name INSTR_EXPR_CLOSE) string_oneline_content_parsed? ;
string_mline : STRING_MLINE_START NEWLINE INC_INDENT string_mline_content+ DEC_INDENT ;
string_mline_content : string_mline_content_parsed NEWLINE | INC_INDENT string_mline_content+ DEC_INDENT;
string_mline_content_parsed : (STRING_MLINE_CONTENT | STRING_MLINE_EXPR known_name INSTR_EXPR_CLOSE) string_mline_content_parsed? ;
str_concat: (string | known_name) (AMP str_concat)?;

expr_bool: expr_or;
expr_or: expr_and ((OR | XOR) expr_and)* ;
expr_and: expr_not (AND expr_not)*;
expr_not: NOT expr_not | expr_bool_atom;
expr_bool_atom: bool_literal | expr_compare | known_name | OPEN_PAREN expr_bool CLOSE_PAREN;
expr_compare: left=expr_calc expr_comp_op right=expr_calc;
expr_comp_op: (EQ | LT | GT | GT_EQ | LT_EQ | NEQ1 | NEQ2);

expr_calc: expr_plus;
expr_plus: left=expr_mul ((PLUS | MINUS) expr_mul)*;
expr_mul: left=expr_pow ((MUL | DIV | IDIV | MOD) expr_pow)*;
expr_pow: left=expr_negate ((POWER | POWER2) expr_negate)*;
expr_negate : MINUS expr_negate | expr_atom;
expr_atom: number | known_name | OPEN_PAREN expr_calc CLOSE_PAREN;

number : NUMBER ;
bool_literal: TRUE | FALSE;
