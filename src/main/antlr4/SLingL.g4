lexer grammar SLingL;

tokens { INC_INDENT, DEC_INDENT }
channels { SPACES }
@header {
    import java.util.LinkedList;
	import java.util.Stack;
}
@members {
    // A queue where extra tokens are pushed on (see the NEWLINE lexer rule).
    protected final LinkedList<Token> tokensQueue = new LinkedList<>();

    // The var that keeps track of the indentation level.
    protected int indentLevel = 0;

    protected boolean atLineStart() {return prevToken == null || prevToken.getType() == NEWLINE || prevToken.getType() == DEC_INDENT || prevToken.getType() == INC_INDENT;}
	protected boolean atLineEnd() {return _input.LA(1) == '\n' || _input.LA(1) == '\r';}

    protected void enqueueToken(Token t) {tokensQueue.add(t);}

    // remeber previous token
    protected Token prevToken = null;
    @Override
    public void emit(Token token) {
        prevToken = token;
        super.emit(token);
    }

    // remeber indent level of modes
    protected Stack<Integer> modeLevelStack = new Stack<>();
	@Override
	public void pushMode(int m) {
		super.pushMode(m);
		modeLevelStack.push(indentLevel);
	}
	@Override
	public int popMode() {
		modeLevelStack.pop();
		return super.popMode();
	}

    // emit approprate amount of indent/dedent tokens and popModes when needed
    protected void levelIndents() {
        int newLevel = countTrailingTabs(getText());
        for (; indentLevel < newLevel; indentLevel++) emitIndDedToken(INC_INDENT);
        for (; indentLevel > newLevel; indentLevel--) emitIndDedToken(DEC_INDENT);
        int modeIndent = modeLevelStack.empty() ? indentLevel : modeLevelStack.peek();
        if (_mode != DEFAULT_MODE && indentLevel <= modeIndent)
            popMode();
    }

    protected int countTrailingTabs(String text) {
        int firstTab = text.indexOf('\t');
        return firstTab < 0 ? 0 : text.length() - firstTab;
    }

    protected void emitIndDedToken(int type) {
        CommonToken token = newToken(type, getText());
        token.setLine(getLine());
        enqueueToken(token);
    }

    protected CommonToken newToken(int type, String text) {
        int stop = this.getCharIndex() - 1;
        int start = text.isEmpty() ? stop : stop - text.length() + 1;
        return new CommonToken(this._tokenFactorySourcePair, type, DEFAULT_TOKEN_CHANNEL, start, stop);
    }

    @Override
    public Token nextToken() {
        while (true) {
            if (!tokensQueue.isEmpty()) {
                Token token = tokensQueue.poll();
                emit(token);
                return token;
            }
            Token next = super.nextToken();
            if (next != null) {
                switch (next.getType()) {
                    case EOF:
                        enqueueToken(newToken(NEWLINE, "\n"));
                        levelIndents();
                        enqueueToken(next);
                        continue;
                    case NEWLINE:
                        levelIndents();
                        break;
                }
            }
            return next;
        }
    }
}

// Spaces and formatting
NEWLINE: ('\r'? '\n' | '\r' | '\f' )+ '\t'* ;
SP : [ ]+ -> channel(SPACES) ;

// Basics
ID : ID_START ID_CONTINUE* ;

// Literals
IS: 'is' ;
A: 'a' ;
AN: 'an' ;
AS: 'as' ;
ARE: 'are' ;
IN: 'in' ;
OF: 'of' ;
HAS: 'has' ;
HAVE: 'have' ;
FOR: 'for' ;
WHEN: 'when' ;
WHENNOT: 'whenNot' ;
WITH: 'with' ;

// punctuations
EXCLAMARK : '!';
QUESTMARK: '?';
OPEN_PAREN: '(';
CLOSE_PAREN: ')';
COMMA : ',';
//DOLLAR: '$';
//COLON : ':';
//SEMI_COLON : ';';
//AT : '@';

//UNKNOWN_CHAR : . ;

// comments
mode DEFAULT_MODE;
    COMMENT_START: HASH {atLineStart()}? -> pushMode(COMMENT);
    COMMENT_ON_EOL : HASH {!atLineStart()}? TEXT;
mode COMMENT;
    COMMENT_TEXT: TEXT;
    COMMENT_NEWLINE : NEWLINE -> type(NEWLINE);

// Strings
mode DEFAULT_MODE;
    STRING_ONELINE_START: DQUOTE SP* {!atLineEnd()}? -> pushMode(STRING_ONELINE);
    STRING_MLINE_START: DQUOTE {atLineEnd()}? -> pushMode(STRING_MLINE);
    fragment STRING_EXPR_INTERP: '$(';
    fragment STRING_ESCAPED_CHAR: '\\.';
    fragment STRING_CHAR : ~('\\'|'\n'|'$'|'"');
mode STRING_ONELINE;
    STRING_ONELINE_CONTENT: (STRING_CHAR | STRING_ESCAPED_CHAR)+;
    STRING_ONELINE_EXPR: STRING_EXPR_INTERP -> pushMode(INSTR_EXPR);
    STRING_ONELINE_CLOSE: DQUOTE -> popMode;
mode STRING_MLINE;
    STRING_MLINE_CONTENT: (STRING_CHAR | DQUOTE | STRING_ESCAPED_CHAR)+;
    STRING_MLINE_EXPR: STRING_EXPR_INTERP -> pushMode(INSTR_EXPR);
    STRING_MLINE_NEWLINE : NEWLINE -> type(NEWLINE);
mode INSTR_EXPR;
    INSTR_EXPR_CLOSE : ')' -> popMode;
    INSTR_EXPR_ID : ID -> type(ID);

// Numbers
mode DEFAULT_MODE;
NUMBER : INTEGER | FLOAT_NUMBER ;
INTEGER : (NON_ZERO_DIGIT DIGIT* | '0') ;
FLOAT_NUMBER : POINT_FLOAT | EXPONENT_FLOAT ;

// Expresstions
mode DEFAULT_MODE;
OR: 'or';
XOR: 'xor';
AND: 'and';
NOT: 'not';
NONE : 'none';
TRUE : 'true';
FALSE : 'false';
//DOT : '.';
AMP : '&';
MUL : '*';
DIV : '/';
MOD : '%';
IDIV : '//';
POWER : '**';
POWER2 : '^';
PLUS : '+';
MINUS : '-';
OPEN_BRACK : '[' ;
CLOSE_BRACK : ']' ;
OPEN_BRACE : '{' ;
CLOSE_BRACE : '}' ;
EQ : '=';
LT : '<';
GT : '>';
GT_EQ : '>=';
LT_EQ : '<=';
NEQ1 : '<>';
NEQ2 : '!=';
QUOTE:  '\'';
DQUOTE:  '"';
    //ARROW : '->';

// fragments
fragment TEXT: CHAR+ ;
fragment CHAR: ~[\r\n\f] ;
fragment STRING_ESCAPE_SEQ : '\\' . ;
fragment NON_ZERO_DIGIT : [1-9] ;
fragment DIGIT : [0-9_] ;
fragment POINT_FLOAT : INT_PART? FRACTION | INT_PART '.' ;
fragment EXPONENT_FLOAT : (INT_PART | POINT_FLOAT) EXPONENT ;
fragment INT_PART : DIGIT+ ;
fragment FRACTION : '.' DIGIT+ ;
fragment EXPONENT : [eE] [+-]? DIGIT+ ;
fragment BYTES_ESCAPE_SEQ : '\\' [\u0000-\u007F] ;
fragment ID_START : [A-Z];
fragment ID_CONTINUE : ID_START | [0-9a-z_] ;
fragment HASH: '#';
