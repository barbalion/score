package org.scorefw.dsl.parsed

/**
 * Base class for all String parts and expressions
 */
sealed trait ExprStringPart extends ExprPart {
  /**
   * Should return one-level-more intended string part, override in descendants
   *
   * @return one-level-more intended self
   */
  def indented: ExprStringPart
}

/**
 * Tool object with some methods
 */
object ExprStringPart {
  /**
   * Tab ('\t') constant StringPart
   */
  def tab(src: ParsedModelSource): StringConstPart = StringConstPart(src, "\t")

  /**
   * Indents the string part, by adding tab (#9) character to all the lines in it
   *
   * @param s the String Part to indent
   * @return One-level-more indented StringPart
   */
  def indent(s: ExprStringPart): ExprStringPart = StringConcatPart.concat(List(ExprStringPart.tab(s.src), s.indented), "")
}

/**
 * Referenced known property as part of String expression
 *
 * @param ref the reference
 */
case class StringRefPart(src: ParsedModelSource, ref: KnownNamePart) extends ExprStringPart {
  override def indented: ExprStringPart = this
}

/**
 * Constant String part of string expressions
 *
 * @param str the string
 */
case class StringConstPart(src: ParsedModelSource, str: String) extends ExprStringPart {
  override def indented: ExprStringPart = StringConstPart(src, str.replaceAll("\\n|\\r\\n", "$0\t"))

  override def toString: String =
    "StringConstPart(\"%s\")".format(str
      .replaceAll("\\\\", "\\\\\\")
      .replaceAll("\n", "\\\\n")
      .replaceAll("\r", "\\\\r")
      .replaceAll("\t", "\\\\t")
    )
}

/**
 * Concatenated string parts
 *
 * @param left  left-side string part
 * @param right right-side string part
 */
case class StringConcatPart(src: ParsedModelSource, left: ExprStringPart, right: ExprStringPart) extends ExprStringPart {
  override def indented: ExprStringPart = StringConcatPart(src, left.indented, right.indented)
}

object StringConcatPart {
  /**
   * Concatenate String parts with a delimiter
   *
   * @param parts list of string parts to concatenate
   * @param delim the delimiter
   * @return concatenated StringPart
   */
  def concat(parts: Iterable[ExprStringPart], delim: String): ExprStringPart = {
    parts.reduce((p, n) => (p, n) match {
      case (s1: StringConstPart, s2: StringConstPart) => StringConstPart(s2.src, s1.str + delim + s2.str)
      case _ => StringConcatPart(n.src, p, n)
    })
  }

  /**
   * Concatenate String parts with '\n' delimiter
   *
   * @param parts list of string parts to concatenate
   * @return concatenated StringPart
   */
  def concatLn(parts: Iterable[ExprStringPart]): ExprStringPart = StringConcatPart.concat(parts, "\n")
}
