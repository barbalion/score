package org.scorefw.dsl.parsed

import org.antlr.v4.runtime.RuleContext

/**
 * Base class for all parsed model parts
 */
trait ParsedModelPart {
  val src: ParsedModelSource
}

case class ParsedModelSource(line: Int, pos: Int) {
  override def toString: String = "%d:%d".format(line, pos)
}

/**
 * Tool class to hold context of unhandled (incorrect) rule
 *
 * @param ctx the context
 */
case class UnhandledRule(src: ParsedModelSource, ctx: RuleContext) extends ParsedModelPart

/**
 * Root of parsed model
 *
 * @param items the list of declarations in the parsed model
 */
case class ParsedModel(src: ParsedModelSource, items: List[DeclPart]) extends ParsedModelPart

/**
 * Base class for all declarations
 */
sealed trait DeclPart extends ParsedModelPart

/**
 * Reference to a newly declared name in the declaration
 *
 * @param name     the name of the Aspect
 * @param modifier '!' or '?' modifier of the declaration
 */
case class NewNamePart(src: ParsedModelSource, name: NamePart, modifier: Option[NewNameModifier]) extends ParsedModelPart

/**
 * 'is' declaration
 *
 * @param newName    newly declared Aspect
 * @param knownNames reference to previously known Aspects
 * @param body       the declaration body
 */
case class IsDeclPart(src: ParsedModelSource, newName: NewNamePart, knownNames: KnownNamesPart, body: IsBodyPart) extends DeclPart

/**
 * Body of 'is' declaration body
 *
 * @param items     parameters of the declaration
 * @param whenOrNot 'when/whenNot' part
 */
case class IsBodyPart(src: ParsedModelSource, items: List[VarValuePart], whenOrNot: Option[WhenOrNotPart]) extends ParsedModelPart

/**
 * Base class of the members for 'is' declaration
 */
sealed trait WithItemPart extends ParsedModelPart

/**
 * Base class for 'when' or 'whenNot' part
 */
trait WhenOrNotPart extends WithItemPart

/**
 * 'when' part
 *
 * @param cond boolean expression
 */
case class WhenPart(src: ParsedModelSource, cond: ExprBoolPart) extends WhenOrNotPart

/**
 * 'whenNot' part
 *
 * @param cond boolean expression
 */
case class WhenNotPart(src: ParsedModelSource, cond: ExprBoolPart) extends WhenOrNotPart

/**
 * Alias declaration
 *
 * @param newName   alias name
 * @param knownName previously known Aspect
 */
case class AliasDeclPart(src: ParsedModelSource, newName: NamePart, knownName: KnownNamePart) extends WithItemPart with DeclPart

/**
 * Parameter of 'is' declaration
 *
 * @param name  name of the parameter
 * @param value value of the parameter
 */
case class VarValuePart(src: ParsedModelSource, name: KnownNamePart, value: ExprPart) extends ParsedModelPart

/**
 * 'has' declaration
 *
 * @param name    the name of the owner Aspect
 * @param members the new members
 */
case class HasDeclPart(src: ParsedModelSource, name: KnownNamePart, members: List[DeclPart]) extends DeclPart

case class HasBodyPart(src: ParsedModelSource, items: List[DeclPart]) extends ParsedModelPart

/**
 * List of references to previously known Aspect
 *
 * @param names the references
 */
case class KnownNamesPart(src: ParsedModelSource, names: List[KnownNamePart]) extends ParsedModelPart {
  def this(src: ParsedModelSource, name: KnownNamePart) = this(src, name :: Nil)
}

object KnownNamesPart {
  def empty(src: ParsedModelSource): KnownNamesPart = KnownNamesPart(src, Nil)
}

/**
 * Reference to previously known Aspect
 *
 * @param names names part
 */
case class KnownNamePart(src: ParsedModelSource, names: List[NamePart]) extends ParsedModelPart

/**
 * A single word in Aspect reference
 *
 * @param name string name
 */
case class NamePart(src: ParsedModelSource, name: String) extends ParsedModelPart

/**
 * Basic class of newly declared Aspect modifiers: '!' or '?'
 */
sealed trait NewNameModifier

/**
 * Newly declared Aspect modifiers: '!' or '?'
 */
object NewNameModifier {
  /**
   * '?' modifier
   */
  val knownName: NewNameModifier = new NewNameModifier() {}
  /**
   * '!' modifier
   */
  val newName: NewNameModifier = new NewNameModifier() {}
}

/**
 * Base class of Expressions
 *
 * @see ExprBoolPart
 * @see ExprArithPart
 */
trait ExprPart extends ParsedModelPart

/**
 * General untyped reference to known property
 *
 * @param names reference
 */
case class ExprUntypedRefPart(src: ParsedModelSource, names: KnownNamesPart) extends ExprPart
