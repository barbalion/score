package org.scorefw.dsl.parsed

import org.antlr.v4.runtime.Token
import org.scorefw.dsl.gen.SLingL

import scala.util.Try

/**
 * Base class of Arithmetic expression parts
 */
sealed trait ExprArithPart extends ExprPart

/**
 * Base class of Atoms of Arithmetic expressions
 */
sealed trait ExprArithAtom extends ExprArithPart

/**
 * Arithmetic constant number
 */
sealed trait ExprArithConstant extends ExprArithAtom

object ExprArithConstant {
  def apply(src: ParsedModelSource, raw: String): ExprArithConstant = {
    val s = raw.replaceAll("_", "")

    def tryConv[T](conv: String => T, const: T => ExprArithConstant): Option[ExprArithConstant] = Try(conv(s)).map(const(_)).toOption

    tryConv(_.toInt, (value: Int) => ExprArithIntConstant.apply(src, value))
      .orElse(tryConv(_.toDouble, (value: Double) => ExprArithFloatConstant.apply(src, value)))
      //      .orElse(tryConv(BigInt.apply, ExprArithIntConstant.apply))
      .getOrElse(throw new RuntimeException("Unable to parse number: " + s))
  }
}

/**
 * Integer constant of Arithmetic expressions
 */
case class ExprArithIntConstant(src: ParsedModelSource, value: Int) extends ExprArithConstant

/**
 * Float constant of Arithmetic expressions
 */
case class ExprArithFloatConstant(src: ParsedModelSource, value: Double) extends ExprArithConstant

/**
 * Atom of Arithmetic expression
 *
 * @param ref atomic reference to a variable
 */
case class ExprArithRefPart(src: ParsedModelSource, ref: KnownNamePart) extends ExprArithAtom

/**
 * Arithmetic expression
 *
 * @param left  left side of the expression
 * @param op    operation
 * @param right right side of the expression
 */
case class ExprArithOperation(src: ParsedModelSource, left: ExprArithPart, op: ExprArithOp, right: ExprArithPart) extends ExprArithPart

/**
 * Base class of comparing operations
 */
sealed abstract class ExprArithOp(name: String) {
  override def toString: String = name
}

/**
 * Arithmetic operations
 */
object ExprArithOp {

  object PLUS extends ExprArithOp("PLUS")

  object MINUS extends ExprArithOp("MINUS")

  object MUL extends ExprArithOp("MUL")

  object DIV extends ExprArithOp("DIV")

  object MOD extends ExprArithOp("MOD")

  object POWER extends ExprArithOp("POWER")

  //noinspection SpellCheckingInspection
  object IDIV extends ExprArithOp("IDIV")

  /**
   * get Operation from Lexer Token type
   *
   * @param token token to check
   * @return appropriate ExprArithOp
   */
  def apply(token: Token): ExprArithOp = token.getType match {
    case SLingL.PLUS => PLUS
    case SLingL.MINUS => MINUS
    case SLingL.MUL => MUL
    case SLingL.DIV => DIV
    case SLingL.IDIV => IDIV
    case SLingL.MOD => MOD
    case SLingL.POWER => POWER
    case _ => Transformer.neverGetHere
  }
}
