package org.scorefw.dsl.parsed

/**
 * Base class of Boolean expression parts
 */
sealed trait ExprBoolPart extends ExprPart

/**
 * Base class of Atoms of Boolean expressions
 */
case class ExprBoolConstant(src: ParsedModelSource, value: Boolean) extends ExprBoolPart

/**
 * Atom of Boolean expression
 *
 * @param ref atomic reference to a variable
 */
case class ExprBoolRefPart(src: ParsedModelSource, ref: KnownNamePart) extends ExprBoolPart

/**
 * 'and' part of boolean expression
 *
 * @param left  left part
 * @param right right part
 */
case class ExprBoolAndPart(src: ParsedModelSource, left: ExprBoolPart, right: ExprBoolPart) extends ExprBoolPart

/**
 * 'or' part of boolean expression
 *
 * @param left  left part
 * @param right right part
 */
case class ExprBoolOrPart(src: ParsedModelSource, left: ExprBoolPart, right: ExprBoolPart) extends ExprBoolPart

/**
 * 'xor' part of boolean expression
 *
 * @param left  left part
 * @param right right part
 */
case class ExprBoolXorPart(src: ParsedModelSource, left: ExprBoolPart, right: ExprBoolPart) extends ExprBoolPart

/**
 * 'not' part of boolean expression
 *
 * @param expr expression
 */
case class ExprBoolNotPart(src: ParsedModelSource, expr: ExprBoolPart) extends ExprBoolPart

/**
 * Comparing part of boolean expression
 *
 * @param left  left side of the comparison
 * @param op    operation
 * @param right right side of the comparison
 */
case class ExprBoolCompare(src: ParsedModelSource, left: ExprArithPart, op: ExprBoolCompareOp, right: ExprArithPart) extends ExprBoolPart

/**
 * Base class of comparing operations
 */
sealed abstract class ExprBoolCompareOp(name: String) extends ParsedModelPart {
  override lazy val src: ParsedModelSource = throw new RuntimeException("Illegal call")

  override def toString: String = name
}

/**
 * Comparing operations
 */
object ExprBoolCompareOp {

  case object EQ extends ExprBoolCompareOp("EQ")

  case object NEQ extends ExprBoolCompareOp("NEQ")

  case object LT extends ExprBoolCompareOp("LT")

  case object GT extends ExprBoolCompareOp("GT")

  case object LTE extends ExprBoolCompareOp("LTE")

  case object GTE extends ExprBoolCompareOp("GTE")

}
