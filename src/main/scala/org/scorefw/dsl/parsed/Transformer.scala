package org.scorefw.dsl.parsed

import java.util

import org.antlr.v4.runtime.tree.{AbstractParseTreeVisitor, ParseTree, TerminalNode}
import org.antlr.v4.runtime.{ParserRuleContext, RuleContext, Token}
import org.scorefw.dsl.gen.SLingP._
import org.scorefw.dsl.gen._

import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}

//noinspection SpellCheckingInspection
//scalastyle:off method.name
//scalastyle:off number.of.methods
object Transformer extends AbstractParseTreeVisitor[ParsedModelPart] with SLingPVisitor[ParsedModelPart] {
  /**
   * Walks the AST and returns the nitty Parsed Model for you
   **/
  def apply(parser: SLingP): ParsedModel = parser.parse

  /**
   * Throws exception on internal errors of the parser. Fix the parser if you get it.
   */
  def neverGetHere: Nothing = throw new RuntimeException("Parser is broken.")

  /**
   * Throws exception on internal errors of the parser. Fix the parser if you get it.
   */
  private def die(ctx: RuleContext) = throw new RuntimeException("Parser is broken. Unrecognized context: " + ctx.getText)

  /**
   * Convert Java Lists to Scala Lists
   */
  private implicit def javaListToScalaList[T](ts: util.List[T]): List[T] = ts.asScala.toList

  /**
   * Add some pretty recursion handling to recursive contexts
   */
  private implicit def richContext[T <: RuleContext](ctx: T): RichContext[T] = RichContext(ctx)

  private case class RichContext[T <: RuleContext](ctx: T) {
    def recurToList(root: T, recur: T => T): List[T] = root :: Option(recur(root)).map(recurToList(_, recur)).getOrElse(Nil)

    def recursive(recur: T => T): List[T] = recurToList(ctx, recur)
  }

  /**
   * Implicitly convert Context to Option of Model Part is possible
   */
  private implicit def conv[T <: ParseTree, TT <: ParsedModelPart](ctx: T)(implicit f: T => TT): Option[TT] =
    Try(Option(ctx).map(f)) match {
      case Success(value) => value
      case Failure(ex) => throw new RuntimeException("Parsing exception near " + ctx.src, ex)
    }

  /** Choose first non-empty Option. Die if it happend to be UnhandledRule
   * */
  private def choose[T](options: Option[T]*): Option[T] =
    options.flatten.headOption

  /**
   * Choose the first option with non-empty first value in the Tuple
   *
   * @return returns the _2 of the chosen Tuple
   */
  private def choose2[T](options: (Any, T)*): Option[T] =
    choose(options.map(o => {Option(o._1).map(_ => o._2) }): _*)

  /**
   * Provides source reference for the context
   */
  private implicit def ctxToSrcConv(ctx: ParserRuleContext): ParsedModelSource =
    ParsedModelSource(ctx.start.getLine, ctx.start.getCharPositionInLine)

  // override all the Visitor's methods with 'implicit' modifier to provide conversions to friendly methods
  override implicit def visitParse(ctx: ParseContext): ParsedModel = {
    ParsedModel(ctx, visitClauses(ctx.clause))
  }

  private def visitClauses(clauses: util.List[ClauseContext]) = clauses.filter(_.comment == null).map(visitClause)

  override implicit def visitIs_decl(ctx: Is_declContext): IsDeclPart = choose(
    ctx.a_single_decl,
    ctx.abracketb_decl,
    ctx.aisb_decl,
    ctx.bfora_decl,
  ).getOrElse(die(ctx))

  override implicit def visitA_single_decl(ctx: SLingP.A_single_declContext): IsDeclPart =
    IsDeclPart(ctx, ctx.new_name, KnownNamesPart.empty(ctx), ctx.is_body)

  //noinspection DuplicatedCode
  override implicit def visitAisb_decl(ctx: Aisb_declContext): IsDeclPart =
    IsDeclPart(ctx, ctx.new_name, ctx.known_names, ctx.is_body)

  override implicit def visitKnown_name(ctx: Known_nameContext): KnownNamePart =
    KnownNamePart(ctx, ctx.recursive(_.known_name).flatMap(ctx => conv(ctx.name)))

  override implicit def visitNew_name(ctx: New_nameContext): NewNamePart =
    NewNamePart(ctx, ctx.name,
      choose2(
        (ctx.EXCLAMARK, Some(NewNameModifier.newName)),
        (ctx.QUESTMARK, Some(NewNameModifier.knownName)),
        (None, None)
      ).getOrElse(die(ctx))
    )

  //noinspection DuplicatedCode
  override implicit def visitBfora_decl(ctx: Bfora_declContext): IsDeclPart =
    IsDeclPart(ctx, ctx.new_name, KnownNamesPart(ctx, List(ctx.known_name)), ctx.is_body)

  //noinspection DuplicatedCode
  override implicit def visitAbracketb_decl(ctx: Abracketb_declContext): IsDeclPart =
    IsDeclPart(ctx, ctx.new_name, ctx.known_names, ctx.is_body)

  override implicit def visitKnown_names(ctx: Known_namesContext): KnownNamesPart =
    KnownNamesPart(ctx, ctx.recursive(_.known_names).flatMap(ctx => conv(ctx.known_name)))

  override implicit def visitIs_body(ctx: Is_bodyContext): IsBodyPart = choose(
    ctx.is_body_oneline,
    ctx.is_body_mline,
  ).getOrElse(die(ctx))

  override implicit def visitIs_body_oneline(ctx: Is_body_onelineContext): IsBodyPart =
    IsBodyPart(ctx, ctx.with_vars_oneline.map(_.items).getOrElse(Nil), ctx.whenOrNot)

  override implicit def visitWith_vars_oneline(ctx: With_vars_onelineContext): IsBodyPart =
    IsBodyPart(ctx, ctx.recursive(_.with_vars_oneline).flatMap(_.with_var), None)

  override implicit def visitIs_body_mline(ctx: Is_body_mlineContext): IsBodyPart =
    IsBodyPart(ctx, ctx.with_varline.filter(_.comment == null).map(visitWith_varline), ctx.whenOrNot)

  override implicit def visitWhen(ctx: WhenContext): WhenOrNotPart =
    WhenPart(ctx, ctx.expr_bool)

  override implicit def visitWhenNot(ctx: WhenNotContext): WhenOrNotPart =
    WhenNotPart(ctx, ctx.expr_bool)

  override implicit def visitWith_var(ctx: With_varContext): VarValuePart =
    VarValuePart(ctx, ctx.known_name, ctx.value)

  override implicit def visitWith_var_str(ctx: With_var_strContext): VarValuePart =
    VarValuePart(ctx, ctx.known_name, ctx.string_mline)

  override implicit def visitWith_varline(ctx: With_varlineContext): VarValuePart = choose(
    ctx.with_var,
    ctx.with_var_str,
  ).getOrElse(die(ctx))

  override implicit def visitAhasb_decl(ctx: Ahasb_declContext): HasDeclPart =
    HasDeclPart(ctx, ctx.known_name, ctx.has_body.items)

  override implicit def visitBina_decl(ctx: Bina_declContext): HasDeclPart =
    HasDeclPart(ctx, ctx.known_name,
      List(
        IsDeclPart(ctx,
          ctx.new_name,
          ctx.known_names.getOrElse(KnownNamesPart.empty(ctx)),
          ctx.is_body)))

  override implicit def visitWhenOrNot(ctx: WhenOrNotContext): WhenOrNotPart = choose(
    ctx.when,
    ctx.whenNot,
  ).getOrElse(die(ctx))

  override implicit def visitAlias_decl(ctx: Alias_declContext): AliasDeclPart =
    AliasDeclPart(ctx, ctx.name, ctx.known_name)

  override implicit def visitHas_decl(ctx: Has_declContext): HasDeclPart = choose(
    ctx.ahasb_decl,
    ctx.bina_decl,
  ).getOrElse(die(ctx))

  override implicit def visitHas_body(ctx: Has_bodyContext): HasBodyPart =
    ctx.is_decl.map(isdelc => HasBodyPart(ctx, List(isdelc))).getOrElse(ctx.has_mbody)

  override implicit def visitHas_mbody(ctx: Has_mbodyContext): HasBodyPart =
    HasBodyPart(ctx, visitClauses(ctx.clause))

  override implicit def visitString(ctx: StringContext): ExprStringPart = choose(
    ctx.string_mline,
    ctx.string_oneline,
  ).getOrElse(die(ctx))

  override implicit def visitString_oneline(ctx: String_onelineContext): ExprStringPart =
    ctx.string_oneline_content_parsed.getOrElse(StringConstPart(ctx, ""))

  override implicit def visitString_mline(ctx: String_mlineContext): ExprStringPart =
    StringConcatPart.concatLn(ctx.string_mline_content.map(visitString_mline_content))

  override implicit def visitString_mline_content(ctx: String_mline_contentContext): ExprStringPart =
    ctx.string_mline_content_parsed.getOrElse(
      StringConcatPart.concatLn(ctx.string_mline_content.map(visitString_mline_content).map(ExprStringPart.indent))
    )

  private def unEscape(s: String): String = scala.StringContext.processEscapes(s)

  override implicit def visitString_oneline_content_parsed(ctx: String_oneline_content_parsedContext): ExprStringPart =
    StringConcatPart.concat(ctx.recursive(_.string_oneline_content_parsed).map(ctx =>
      Option(ctx.STRING_ONELINE_CONTENT).map(s => StringConstPart(ctx, unEscape(s.getText)))
        .orElse(ctx.known_name.map(StringRefPart(ctx, _)))
        .getOrElse(die(ctx))
    ), "")

  override implicit def visitString_mline_content_parsed(ctx: String_mline_content_parsedContext): ExprStringPart =
    StringConcatPart.concat(ctx.recursive(_.string_mline_content_parsed).map(ctx =>
      Option(ctx.STRING_MLINE_CONTENT).map(s => StringConstPart(ctx, unEscape(s.getText)))
        .getOrElse(Option(StringRefPart(ctx, ctx.known_name)).getOrElse(die(ctx)))
    ), "")

  override implicit def visitStr_expr(ctx: Str_exprContext): ExprStringPart = choose(
    ctx.string,
    ctx.str_concat
  ).getOrElse(die(ctx))

  override implicit def visitStr_concat(ctx: Str_concatContext): ExprStringPart =
    StringConcatPart.concat(
      ctx.recursive(_.str_concat).map(ctx => {
        choose(
          ctx.known_name.map(StringRefPart(ctx, _)),
          Option(ctx.str_concat).map(visitStr_concat) // no implicits in recursion
        ).getOrElse(die(ctx))
      }), "")

  override implicit def visitName(ctx: NameContext): NamePart =
    NamePart(ctx, ctx.getText)

  override implicit def visitClause(ctx: ClauseContext): DeclPart = choose(
    ctx.is_decl,
    ctx.has_decl,
    ctx.alias_decl,
  ).getOrElse(die(ctx))

  override implicit def visitValue(ctx: ValueContext): ExprPart = choose(
    ctx.bool_literal,
    ctx.number,
    ctx.known_name.map(n => ExprUntypedRefPart(ctx, KnownNamesPart(ctx, List(n)))),
    ctx.string,
    ctx.expr,
  ).getOrElse(die(ctx))

  override implicit def visitExpr(ctx: ExprContext): ExprPart = choose(
    ctx.expr_bool,
    ctx.expr_calc,
    ctx.str_expr,
  ).getOrElse(die(ctx))

  override implicit def visitExpr_bool(ctx: Expr_boolContext): ExprBoolPart =
    ctx.expr_or

  override implicit def visitExpr_or(ctx: Expr_orContext): ExprBoolPart =
    ctx.expr_and.map(visitExpr_and).reduce((left, right) =>
      Option(ctx.OR)
        .map(_ => ExprBoolOrPart(ctx, left, right))
        .getOrElse(ExprBoolXorPart(ctx, left, right)))

  override implicit def visitExpr_and(ctx: Expr_andContext): ExprBoolPart =
    ctx.expr_not.map(visitExpr_not).reduce((left, right) => ExprBoolOrPart(ctx, left, right))

  override implicit def visitExpr_not(ctx: Expr_notContext): ExprBoolPart = choose(
    ctx.expr_bool_atom,
    Option(ctx.expr_not).map(ctx => ExprBoolNotPart(ctx, visitExpr_not(ctx)))
  ).getOrElse(die(ctx))

  override implicit def visitExpr_bool_atom(ctx: Expr_bool_atomContext): ExprBoolPart = choose(
    ctx.bool_literal,
    ctx.expr_compare,
    ctx.known_name.map(ExprBoolRefPart(ctx, _)),
    ctx.expr_bool,
  ).getOrElse(die(ctx))

  override implicit def visitBool_literal(ctx: Bool_literalContext): ExprBoolConstant = choose2(
    (ctx.TRUE, ExprBoolConstant(ctx, true)),
    (ctx.FALSE, ExprBoolConstant(ctx, false)),
  ).getOrElse(die(ctx))

  override implicit def visitExpr_compare(ctx: Expr_compareContext): ExprBoolCompare =
    ExprBoolCompare(ctx, ctx.left, visitExpr_comp_op(ctx.expr_comp_op), ctx.right)

  override implicit def visitExpr_comp_op(ctx: Expr_comp_opContext): ExprBoolCompareOp = choose2(
    (ctx.EQ, ExprBoolCompareOp.EQ),
    (ctx.LT, ExprBoolCompareOp.LT),
    (ctx.GT, ExprBoolCompareOp.GT),
    (ctx.GT_EQ, ExprBoolCompareOp.GTE),
    (ctx.LT_EQ, ExprBoolCompareOp.LTE),
    (ctx.NEQ1, ExprBoolCompareOp.NEQ),
    (ctx.NEQ2, ExprBoolCompareOp.NEQ),
  ).getOrElse(die(ctx))

  override implicit def visitExpr_calc(ctx: Expr_calcContext): ExprArithPart =
    ctx.expr_plus

  /**
   * Convert list of operations in the context by iterating over the children
   *
   * @param ctx      the parent context, containing the list (children)
   * @param opResolv the resolver of operation kind
   * @param combiner folding/transforming operation
   * @param f        convertion function of a operation context into transformed class
   * @tparam T  type of the target transformed class
   * @tparam OP type of the operation kind
   * @tparam L  source type of an element in the list
   * @return the final folded operation, combining all the operation in the list
   */
  private def convListOpContext[T <: ParsedModelPart, OP, L <: ParserRuleContext](ctx: ParserRuleContext, opResolv: Token => OP,
                                                                                  combiner: (ParsedModelSource, T, OP, T) => T)(implicit f: L => T): T = {
    ctx.children.toList match {
      case left :: tail =>
        tail.grouped(2).foldLeft(f(left.asInstanceOf[L]))((left, right) => right match {
          case List(op: TerminalNode, right) =>
            val r = f(right.asInstanceOf[L])
            combiner(r.src, left, opResolv(op.getSymbol), r)
          case _ => die(ctx)
        })
      case _ => die(ctx)
    }
  }

  override implicit def visitExpr_plus(ctx: Expr_plusContext): ExprArithPart =
    convListOpContext[ExprArithPart, ExprArithOp, Expr_mulContext](ctx, ExprArithOp.apply, ExprArithOperation)

  override implicit def visitExpr_mul(ctx: Expr_mulContext): ExprArithPart =
    convListOpContext[ExprArithPart, ExprArithOp, Expr_powContext](ctx, ExprArithOp.apply, ExprArithOperation)

  override implicit def visitExpr_pow(ctx: Expr_powContext): ExprArithPart =
    convListOpContext[ExprArithPart, ExprArithOp, Expr_negateContext](ctx, ExprArithOp.apply, ExprArithOperation)

  override implicit def visitExpr_negate(ctx: Expr_negateContext): ExprArithPart =
    Option(ctx.MINUS).map(_ =>
      ExprArithOperation(ctx, ExprArithIntConstant(ctx, 0), ExprArithOp.MINUS, visitExpr_negate(ctx.expr_negate)))
      .getOrElse(ctx.expr_atom)

  override implicit def visitExpr_atom(ctx: Expr_atomContext): ExprArithPart = choose(
    ctx.number,
    ctx.known_name.map(ExprArithRefPart(ctx, _)),
    ctx.expr_calc,
  ).getOrElse(die(ctx))

  override implicit def visitNumber(ctx: NumberContext): ExprArithConstant =
    ExprArithConstant(ctx, ctx.NUMBER.getText)

  // Never parse splitters and primitive rules

  override implicit def visitAs(ctx: AsContext): ParsedModelPart = neverGetHere

  override implicit def visitHas(ctx: HasContext): ParsedModelPart = neverGetHere

  override implicit def visitIn(ctx: InContext): ParsedModelPart = neverGetHere

  override implicit def visitOf(ctx: OfContext): ParsedModelPart = neverGetHere

  override implicit def visitEol(ctx: EolContext): ParsedModelPart = neverGetHere

  override implicit def visitIs(ctx: IsContext): ParsedModelPart = neverGetHere

  override implicit def visitComment(ctx: CommentContext): ParsedModelPart = neverGetHere

  override implicit def visitComment_on_eol(ctx: Comment_on_eolContext): ParsedModelPart = neverGetHere

  override implicit def visitComment_mline(ctx: Comment_mlineContext): ParsedModelPart = neverGetHere

}

