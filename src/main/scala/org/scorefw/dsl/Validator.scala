package org.scorefw.dsl

import java.io.{PrintWriter, StringWriter}

import org.antlr.v4.runtime.{CharStream, CharStreams, CommonTokenStream}
import org.scorefw.dsl.parsed.Transformer
import org.scorefw.dsl.parser.{SLingLexerEx, SLingParserEx, ThrowingErrorListener}
import scopt.OParser

import scala.io.{Codec, Source}
import scala.util.{Failure, Try}

object Validator extends App {

  case class Config
  (
    files: Seq[String] = Nil,
    verbose: Boolean = false,
    debug: Boolean = false,
  )

  val config: Config = {
    val builder = OParser.builder[Config]
    OParser.parse({
      import builder._

      OParser.sequence(
        programName("Score DSL Validator"),
        //noinspection SpellCheckingInspection
        head("chksyntax", "0.1"),
        help('h', "help").text("prints this usage text"),
        arg[String]("<files>...")
          .minOccurs(1)
          .required()
          .action((x, c) => c.copy(files = c.files :+ x))
          .text("files to validate"),
        opt[Unit]('v', "verbose")
          .action((_, c) => c.copy(verbose = true))
          .text("verbose is a flag"),
        opt[Unit]('d', "debug")
          .hidden()
          .action((_, c) => c.copy(debug = true))
          .text("this option is hidden in the usage text"),
      )
    }, args, Config()).getOrElse(sys.exit(1))
  }

  Try(
    config.files.foreach(processFile)
  ) match {
    case Failure(e) =>
      val writer = new StringWriter
      e.printStackTrace(new PrintWriter(writer))
      println("Parsing Error: %s\n%s".format(e, writer.getBuffer))
      sys.exit(1)
    case _ =>
      sys.exit(0)
  }

  //noinspection SameParameterValue
  private def runParser(input: CharStream) = {
    val lexer = new SLingLexerEx(input, config.debug)
    lexer.removeErrorListeners()
    lexer.addErrorListener(ThrowingErrorListener.INSTANCE)
    val tokens = new CommonTokenStream(lexer)
    val parser = new SLingParserEx(tokens, config.debug)
    parser.removeErrorListeners()
    parser.addErrorListener(ThrowingErrorListener.INSTANCE)
    //    parser.setErrorHandler(new ExceptionErrorStrategy)
    parser
  }

  def parse(input: CharStream): Unit = {
    val parser = runParser(input)
    //    print(parser.parse.toStringTree(parser.getRuleNames.toList.asJava))
    val model = Transformer(parser)
    System.out.println(model.toString)
    //    print(tree.toStringTree())
  }

  def processFile(path: String): Unit = {
    val fs = Source.fromFile(path)(Codec.UTF8)
    try {
      parse(CharStreams.fromString(fs.getLines().mkString("\n")))
    }
    finally {
      fs.close()
    }
  }

}
