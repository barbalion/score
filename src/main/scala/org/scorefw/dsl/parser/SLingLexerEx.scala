package org.scorefw.dsl.parser

import org.antlr.v4.runtime.{CharStream, Token}
import org.scorefw.dsl.gen.SLingL

class SLingLexerEx(val input: CharStream, val debug: Boolean = false) extends SLingL(input) {
  override def emit(token: Token): Unit = {
    if (debug) {
      println(String.format("   Lexer emitted (%s) %d:%d %s: \"%s\"",
        getModeNames()(_mode),
        getLine,
        getCharPositionInLine,
        getVocabulary.getDisplayName(token.getType),
        token.getText.replaceAll("\n", "\\\\n").replaceAll("\t", "\\\\t")))
    }
    super.emit(token)
  }
}
