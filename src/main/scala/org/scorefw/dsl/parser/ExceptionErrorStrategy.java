package org.scorefw.dsl.parser;

import org.antlr.v4.runtime.*;

public class ExceptionErrorStrategy extends DefaultErrorStrategy {

    @Override
    public void recover(Parser recognizer, RecognitionException e) {
        throw e;
    }

    @Override
    public void reportError(Parser recognizer, RecognitionException e) {
        throw e;
    }

    @Override
    public void reportInputMismatch(Parser recognizer, InputMismatchException e) throws RecognitionException {
        String msg = formatBadTokenString(recognizer, "mismatched input at %d:%d: %s, expecting one of ") +
                getExpectingString(recognizer);
        throw new RecognitionException(msg, recognizer, recognizer.getInputStream(), recognizer.getContext());
    }

    private String formatBadTokenString(Parser recognizer, String format) {
        Token badToken = recognizer.getCurrentToken();
        int line = badToken.getLine();
        int pos = badToken.getCharPositionInLine();
        return String.format(format, line, pos, getTokenErrorDisplay(badToken));
    }

    private String getExpectingString(Parser recognizer) {
        return recognizer.getExpectedTokens().toString(recognizer.getVocabulary());
    }

    @Override
    public void reportMissingToken(Parser recognizer) {
        String msg = String.format("missing %s at %s", getExpectingString(recognizer), formatBadTokenString(recognizer, "%d:%d: %s"));
        throw new RecognitionException(msg, recognizer, recognizer.getInputStream(), recognizer.getContext());
    }
}
