package org.scorefw.dsl.parser;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;

public class ThrowingErrorListener extends BaseErrorListener {

    public static final ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
            throws ParseCancellationException {
        String badToken = e == null ? "" : e.getOffendingToken() == null ? "<not a token>" : e.getOffendingToken().getText().replaceAll("\n", "\\\\n").replaceAll("\t", "\\\\t");
        throw new ParseCancellationException("at " + line + ":" + charPositionInLine + " found \"" + badToken + "\", but " + msg);
    }

}
