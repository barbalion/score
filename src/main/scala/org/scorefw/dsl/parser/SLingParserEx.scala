package org.scorefw.dsl.parser

import org.antlr.v4.runtime.{ParserRuleContext, TokenStream}
import org.scorefw.dsl.gen.SLingP

class SLingParserEx(input: TokenStream, val debug: Boolean = false) extends SLingP(input) {
  override def enterRule(localctx: ParserRuleContext, state: Int, ruleIndex: Int): Unit = {
    super.enterRule(localctx, state, ruleIndex)
    if (debug) {
      println("Parser entered rule %s at %d:%d".format(
        getRuleInvocationStack(localctx),
        localctx.start.getLine,
        localctx.start.getCharPositionInLine)
      )
    }
  }

  override def exitRule(): Unit = {
    //noinspection SpellCheckingInspection
    val localctx = _ctx
    super.exitRule()
    if (debug && _ctx != null) {
      println("Parser exited rule %s at from %d:%d to %d:%d".format(
        getRuleInvocationStack(localctx),
        localctx.start.getLine, localctx.start.getCharPositionInLine,
        localctx.stop.getLine, localctx.stop.getCharPositionInLine)
      )
    }
  }
}
