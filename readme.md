# Introduction
This is **Score** (stands for "**S**calable/**S**anya's **Core**"), the declarative, reactive, aspect-oriented, domain-driven and event-sourced Framework for business and general application development. 

It is designed to be fully ACID-consistent, SOLID-complaint, unlimitly Scalable, extremely Customizable, and Platform Independent, both on back-end and front-end.

Score is statically-typed and implicitly parallel on every stage of processing **by default**.

Score is shipped with code-generators for JVM back-end and ReactJS front-end.

# Features
Score Framework contains functionality for:
 * Data Model Definitions
 * Workflow Definition
 * Security Enforcement
 * Arbitrary Data Persistence Storage
 
# Main Concept
Your application consists of *Entities*. Every Entity has some particular *Behavior*.
The Behavior is set of *Aspects*. For instance, Fields, Persistence, Serialization, GUI Form, API methods, Workflow, etc.

In other words, to add some particular behavior (methods, properties) to you *Entity*, you need to declare appropriate *Aspect* for it.

# Language
The Score provides you with Declaration Language (SLing, stands for *Score's LINGua* (Latin for "laguage")), the domain-specific language (DSL) for Application declaration.

# Aspects
In contrast to Object Oriented (OOP) definitions, there is no single "super-class" for an *Entity* in Score. Entity is just a *Composition* of all Aspects that have been declared for it.

You can think of **Aspect** as a **mixin**, a **trait**, a **plug-in**, which you can add to the Entity for it to obtain some behavior (properties, functionality). In some sense Aspect injection solves the same problem as *type-classes* and *delegates* in Scala3, `impl ... for ...` in Rust, *extensions/shapes* in Kotlin, C#, *protocols* in Swift and *annotation* in many languages do - the meta-programming plus **deferred inheritance**.

By default your *Entity* is completely empty, has no behavior at all. There is no special syntax to define a new Entity. It is born with its first declaration, a declaration of its first Aspect.

	Foo
	
Entity/Aspect `Foo` is born.

# Declaration file Syntax
You define your Application is a set of `.sld` files (SLing Declaration File). 
Every file is a set of declarations. There is no special syntax for the header or for the file ending.
Any number or empty lines can divide different declarations in the file.

The order of the declarations doesn't matter. File and folder structure doesn't matter as well. You are free to split your declarations (even for a single Entity) into as many files as you like. You provide the path to the *Catalog* (parent folder or zip-archives with all your declarations) at boot-time of the Score.

You are free to feed your model into the engine from any other source (even a database).

# Indentation
A multi-line declaration must by *properly indented*. This means that every dependent line is indented for one level more than its owner. This applies to all the declarations, comments and other syntax constructs.

One level on indentation is exactly one `tab` character (ASCII #9). Once again: **indents only by tabs**, single Tab per level, no spaces are allowed in indents. The Holly-War ends here. 

	Here is Declaration
		Here it continues
		... and continues
			here is sub-declaration
			and its continuation
		main declaration ends after this
	And this is another declaration
*) Note: This is not a real declaration syntax. We illustrate the indentation here.

# Comments
It is allowed to have comments. Every comment starts with `#` character and lasts till the end of the line. 

	Foo # and my comment about Foo

If a comment starts in the beginning of the line then all the following properly indented lines are also commented out. Consider the following multi-line comment example:

	# This is a comment
		and this is a continuation of the comment.

# Identifiers
Identifiers and all names are **case-sensitive**. Every identifier has to follow next Regular Expression: `[A-Z][A-Za-z0-9_]+`.

In other words an Identifier starts with a capital letter and continues with letters and digits. 

Words starting with a **lowercase** letter is for Score's internal use only (i.e. **reserved**), don't try to declare such Aspects on your own.

Please note that the underscore character `_` is ignored. It means that `Foo_Bar` is equivalent to `ForBar`, and `_` is not a valid identifier at all. You are encouraged to use "[CamelCased](https://en.wikipedia.org/wiki/Camel_case)" identifiers. 

# Constants
There are reserved words `true` and `false` for the binary constants.

Numeric constants can be written in formats: `123` (integer), `123.45` (floating point), `1e7` or `1E7` (equals to 10000000.0), `0xFF` (hexadecimal integer).
You are free to write numeric constants with underscores for readability. Consider `1_000_000` (what is equivalent to `1000000`), or `0.000_001` (0.000001).

String constants must be enclosed in double or single quotes `"`. Consider: `"This a string"` and `'This is also a valid string'`.

A multi-line string should follow the rules: 
1. starts with a quote `"` or `'`
2. followed by `\n` (end of line) character.
3. every line is properly indented.

The first line with opening quotes is ignored: there will be no extra `\n` character in the string. The basic level of indentation is ignored as well. 
Example:

	MyString "
		Some text goes here.
			And continues here.

The result string will be equal to `"Some text goes here.\n\tAnd continues here."`.

# Expressions
Every expression must be enclosed in brackets. Consider: `(1+1)` equals 2, `(2^8)` equals to *2 in power of 8* = 256, `2**8` is the same as former, `(1.0/2)` equals 0.5, `3//2` (integer division) equals to 1.

Please note that in order for a particular operation to work, the *Domain* must support it. Some domains can forbid the operations which you might be used to.
For instance you cannot divide integers with `/`. Because the result will be of different domain (`float`). So, you have to choose either to use integer division `//` or explicitly convert integer to `float` first.

Another example is that the `Money` Domain does not support division at all, to prevent precision lost. There is `split` operation of `Money` to return a set of partial amounts (total sum of which ones will be equal to the original amount).

# Declarations
As it was said above, to add some behavior to you Entity you need to declare an Aspect for it.
There are two types of declaration clauses: Composition and Injection.

## Composition 
Example:

	A is B
	# what is equivalent to:
	A (B)

This an anonymous injection, what means that `A` gets the same properties and behavior as `B` had. This injection doesn't have a name, that's why it is anonymous. 

The latter syntax (`A (B)`) gives you a shotter way to declare multiple inheritance:

	UserName (String, Required)
	
This is equvalent to 

	UserName is String
	UserName is Required

Now consider:

	A is MyTrait (B)

or

	MyTrait (B) for A

In contrast this injection has a name `MyTrait` and can be accessed by this identifier later. 
The primer example (of anonymous injection) can be accessed by identifier "A&B" or "B.for.A" interchangeably.

## Injection
Injections are for Properties and Methods. Consider:

	A has B

or

	B in A

 and their named counterparts:

	A has MyProperty (B)

and

	MyProperty (B) in A
	
The `has` keyword interchangeable and substitutable with `have`.

## Combined declaration
You can *combine* declarations into one statement as follows:

	A is (B, C)
 
This is equivalent to the set

	A is B
	A is C
 
Usually you will combine injections:

	A has
		MyProp1 (Domain1)
		MyProp2 (Domain2)

Empty lines are not allowed in combined declarations. If you like to visually distinguish set of declarations, then put properly indented comment (single `#` will work just fine) between them.
(TBD)

## Customization and Overriding
Since the order of the declaration is ignored, sometimes you may need to specifiy that your new declaration overrides an old one. In this case you can you the `!` and `?` *modifiers* as follows:

	Document! is Entity 
	
Here `!` means that this is the original declaration of the Aspect. The system will fail in the case of two concurrent declaration with '!' modifier. Otherwise, it's absolutely OK to have
	
	Foo is Bar
	Foo is Bar

The second declaration will not have any effect due to declarative nature of SLing (the warning will be shown, though).
The `Bar` must be declared somewere in your Catalog. An error will be raised otherwise.

Now consider:

	Document.Comment? is ReadOnly
			
The `?` here means that this Aspect must be properly declared somewhere else.

By default Score does not require to use the mentioned modifiers in every declaration. However, 
it is considered a good practice to add `!` to your first apperance of the Aspect. You can control the strictness of the declaration by providing additional parameter to the engine.

## Multiple Inheritance and harnessing the Chaos
Sometimes you will want to limit the multiple inheritance to avoid cases like:

	# First we declare...
	MyField is String
	# later we try to add incompatible...
	MyField is Integer

To fix you should declare your `String` and `Integer` Aspects in the following way:

	String is Domain
	Integer is Domain
	Domain has ThisIsADomain! # just any unique name with '!' modifier

After this trying to compose `String` and `Integer` in one field will fail due to duplication of the `ThisIsAFieldType`. 

Sometimes you will want to make some Aspect to be applicable only 'on the top' of a specific Aspect. For instance built-in `Required` Aspect, which means what it says (same as `not null` in SQL). It must be used only over `Domains`. We declare it in the following way:

	Required has
		ThisIsAFieldType?

Now trying to declare it over inapproriate Aspect (without `ThisIsAFieldType`) will fail. 

In the same way you can create new dependent Aspects, by declaring all the required declarations with `?` modifier.

	SerializableToXML has
		RootElement?
		XMLVersion?
		
## Ordering
Ususally all the Properties will be visualized the order in which they were declared.

	User has
		FirstName (String)
		LastName (String)

If you need to expand the original declaration with a new field, you may properly position it within the old Fields:

	User has
		FirstName?
		MiddleName (String)

This will put `MiddleName` right after `FirstName`.

	User has
		MiddleName (String)
		LastName?
		
This will put `MiddleName` right before `LastName`.

Alternatively you can use built-in predicate `order` for every Field. Order obtains real number, including negative and fractional. Default value is `0`. This means that negative-order Fields will be before Fields without `order` specified. `1.5` will be between `1` and `2`, and so on.

	User has
		FirstName (String) with order 1
		LastName (String) with order 2

	User has
		MiddleName (String) with order 1.5

## Parameters
Often your Aspects have additional parameters. Let's say you want to define a `String` field of an Entity. For instance

	User has
		Name (String)

You would probably want to apply some rules to the name. Let's say maximum length and allowed characters. The `String` Domain supports such parameters:

	User has
		Name (String, Required) with MaxLen 30, AllowedRE "[A-Za-z]\w+"
		
	# or in this way
	User has Name (String, Required) with 
		MaxLen 30
		AllowedRE "[A-Za-z]\w+"

	# or in this way
	User has Name (String, Required) with 
		30 as MaxLen 
		"[A-Za-z]\w+" as AllowedRE

In other words you are free to provide comma-delimited parameters to this injection following `with` keyword. 

The same applies to compositions:

	User is PersistedEntity with
			DB AuthDatabase
			TableName "Users"

	# this is equivalent to 
	User is	PersistedEntity with DB AuthDatabase, TableName "Users"

You are free to provide more parameters in separate declarations:

	User.Nickname is Entity.PrimaryKey     # note the fully qualified name for User.PrimaryKey due to the outer scope
	User.Email is Entity.Unique
	User.Email.Unique with ErrorText 'There is another user with this email'

## Domains
In real live you should never declare a `User.Name` field in the way that we just did. You should always prefer to declare *Data Domains* first. Consider:

	# Domains
	Login is String, MaxLen 30, AllowedRE "[A-Za-z]\w+"
	Password is String, MinLen 8, MaxLen 40
	Password is PersistedBySaltyHash
	EmailAddress is String, AllowedRE=".+\@.+"
	Gender is Enum
	Gender has
		Male (Option)
		Female (Option)
		Other (Option)
	
	# Entites
	User has 
		Login (*, Required)
		Password (*, Required)
		EmailAddress
		Gender

This is the way you follow DRY principle and decrease the amount of code/errors in your application.

## Conditional declaration
There is built-in `when` predicate, which you can use to control application of Aspects. Example:

	User is Editable when PersistedEntity
	User is PersistedEntity when (User->SimpleAuth)
	
Along with `when`, there is `whenNot` predicate. The reason of adding it is the need to respect separate declaration, especially while overriding. Consider:

	#Original Declaration
	Foo is Bar when (Session->Now->DOW=1) # Applies on Moday

	#Overriding Declaration 
	Foo is Bar when (Session->Now->DOW=2) # Applies on Tuesday as well

	#Another overriding Declaration
	Foo is Bar whenNot (Session->Now->Hour>20) # Disables after 8pm
	
Here all the `when` conditions are being calculated using logical `OR` while `whenNot` - using logical `AND`. So, the `Foo` will be `Bar` only on Mondays and Tuesdays before 8pm. In oder to build more complex boolean conditions you are encouraged to use intermediate 'marking' Aspects with meaningful names.

	Foo is Bar when (NowIsProperDay and NowIsProperTime)
	
And override them later appropriatly:
	
	NowIsProperDay when (...)

## Name-spaces
You always have a nested context of Name-spaces. This means that inside a declaration you have access to all its Aspects by their identifiers/names. Consider:

	Foo has
		FooName = 'Foo'
		Bar has
			BarName = 'Bar'
			Baz has
				BazName = 'Baz'
				FullName val (FooName + BarName + BazName) # all the names are accessible here

	ExternallyDeclaredName val (Foo.FooName + Foo.Bar.BarName + Foo.Bar.Baz.BazName)) # you have to provide full path the parts
				
# Relations
Your objects can reference each other. There are two commonly used types of relation: *one-to-many* and *many-to-many*. Let's talk about them separately.
# One-to-many 
One-to-many could be either *Master-Detail* or *Object-Lookup*. They should be treated and declared differently, since the life-circle of a Detail is tied to its Master, while Lookup reference doesn't hold such sematics. Consider the following example showing both types of *one-to-many* relation:

	UserAccount is Entity

	Order is Entity
	Order has
		Buyer (Ref) to UserAccount # This is a Lookup Reference 
		Items (Detail) of Item # This is Master-Detail
			Item has
				Price (Money)
				Quantity (PositiveInteger)
		
		
Since `UserAccount` is an `Entity`, the `Buyer` will be treated as Reference (Entity inside Entity). This will provide the behaviour of lookup-kind reference (dropdown contol, etc.).
`Detail` is a built-in Aspect, providing behaviour to store multiple instances of the `Item` object in the `Order`.

Your are free to split your declaration for the sake of readability in the following way:

	Order has
		Items (Detail) of OrderItem

	OrderItem has
		Price (Money)
		Quantity (PositiveInteger)
		
But in there latter case you should care about name-spaces.

# Many-to-many 
Many-to-many also falls into two different categories: it's either an independent object which have two (or more) Lookup-reference to other objects (symmetric case), or it is considered as a Detail of one of the references Object (asymmetric case). For instance `User's Groups` (many-to-many) is usually triated as a Property of User Object, and not as a property of Group Object (it only references it), but you may have another model.

You should carefully choose the either option for your relation by analyzing you Domain.

# Event Sourcing
Score follows the general idea of [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html). In practice this means that all the Data in the system is a set of immutable pieces. Originally in Event Sourcing a single data change is called an 'Event', but we will avoid this term because of its ambiguity. We prefer to call this facts Datoms, along with [Datomic](datomic.com) platform.

## Datom
Datom is an atomic fact that semantically represents the intent of an **external** agent (user, third-party system, etc.). This can be insert/update/delete of a document, a workflow action, a event from outside. It's persisted at the exact moment when it's obtained.

Since Datoms are immutable (by definition and by their nature), the storage can be extremely simple: it is append-only linear stream of blobs. It can be backed by any (re-playable) streaming Engine: starting from single on-disk-file, ending with [Kafka](kafka.apache.org) Topic. A great by-product of such a storage is the easiness of incremental backups and on-the-fly-compression.

A Datom is immutable, but the business documents are not. There is no contradiction here. Datom (let's say `insert Document with Id=1, Foo="Bar"` or `update Document(Id=1) set Foo="Baz"`) is the *Origin*, while the Document itself is a *Derivative*. This means that having all the Datoms we can easily recreate the Document at any point of time. This is why Datoms are **the only persisted** things in the Application. All the Derivatives **can be** stored for **caching** purpose only. This includes Documents, Lists/Indice, Workflow States, etc. 

## Events/Messages
There is **no such thing** as Event (or Message) in Score. Please get it right: the implementation of Score is driven by events to full extent and has Service-Oriented-Architecture ([SOA](https://en.wikipedia.org/wiki/Service-oriented_architecture)). However, your will never declare messages/events/handlers/dispatchers for you Application in Score's SLing. Event-driven Design is considered to be too low-level and hardly maintainable in long-term, especially in large Applications, since the number of edges (Events) in your dependency graph of nodes (Objects) grows as factorial with the size of your application.

When your Application has to interact with outside event-driven counterparts, use Bridges which transform Event into persisted Datoms in an **idempotent manner**.

The Application cannot internally generate any Datoms (to simulate events) for itself.

You should think of your Application not in terms of Objects+Events, but in terms of *Data Flows* instead.

## Reactive Datasets
All the *Datasets* in Score are [Reactive](https://en.wikipedia.org/wiki/Reactive_programming). This means that they deliver all the changes automatically till the very end of the processing. The important property of Score' Datasets is they are *Lazy* by default. This means that a change in the Dataset (effectively) invalidates all the Derivatives (dependent Datasets). Once a Consumer tries to fetch data from the invalidated Derivative, it will fetch the delta of the changes from all its sources and updates itself. (Behind the scene, every Dataset holds the last value of Data Generation, as the property of data 'freshness' and fetches the changes happend after its previous know generation till the latest generation of the database.)

## Types
Score recognize the following kinds of type:

|Type|Description|Syntax Example|
|--|---|---|
|*Scalar*|regular field's value| `1`, `'string'`, `true`, `(value)`, `Object->PropName`|
|*Tuple*| ordered 'horizontal' set of data constituting a record. Every positioned 'field' of the Tuple has a rich Domain metadata | `| 1 | true |'string' |`, `|field1 | field2 | field3 |`|
|*List*| ordered 'vertical' set of data constituting an array of Tuples (records) | `[1, 2, 3]`, `['one', 'two', 'three']`, `[(1, 'one'), (2, 'two'), (3, 'three')]` |
|*Map*|Dictionary of key-value relations|`{1: 'one', 2: 'two', 3:'three'}`|

There the follwing operations over the types:

|Type|Operations|
|--|--|--|
|*Numeric* (`Integer`, `Real`, `Complex`)| `+`,`-`, `*`, `/`, etc. - standard arithmetics|
|*Boolean* (`Bool`, `Flag`)|`and`, `or`, `not`, `xor` - standard boolean operations|
|*String* (`LongString`, `ShortString`, `Text`)| `str1 | str2` - concatenation, `str*n` - repitation `n` times.
|*Tuple* (an object)| `(|1|) |'str'|` - the concatination resulting in `|1 | 'str' |`|
|*Array* (DataSet's Result)|`[1,2]&[3,4]` - concatenation|
|*Map* ()|`{1:'One'}&{2: Two}}` - concatenation|

A Dataset's result has the type `Table` - *Array of Tuples*. 

## Data Flow
To turn your Datoms into something practical (Documents/Lists/Reports) you need to explain to the system how to process the data. This is being achieved by *Flows*.
The Flows replaces Events/Commands. 

The power of Flows is that their declarative nature allows the engine to run them extremely parallel and memorize every step, effectively utilizing all the available hardware (CPU+RAM).

Let's consider simple model:

	# Model
	Session is Entity
	Session has
		User
		In (TimeStamp)
		Out (TimeStamp)
		Active when (not Out)

	User is Entity
	User has
		Name (UserName)
		Role with AllowMany

	Role is Entity
	Role has 
		Name (ShortString)

Now, a *Flow* uses some *Origins* and provide some *Result*. It can look like this:

	# DataFlow
	LoggedAdmins gives
		S: Session -> 
		A: Active -> 
		R: User.Role -> 
		F: Name = "Administrator" ->
		{User.Name, In}
		
Let's look in the details:
1. `LoggedAdmins` is the name of the new declared *Reactive Dataset*.
1. `gives` is the keyword to declare the flow statement.
1. `S` is an unique *Alias* for later use within the statement. An Alias always is required.
1. `Session` is the name of the source Dataset (the list of all sessions in our case)
1. Arrow operator `->` means 'pass data to'. 
1. `Active` referes to `Session.Active` because the closes Context with `Active` declared is the `Session`. This dataset contains only the active sessions, what in the end effectively leads to the filtering of the owner dataset `Session`.
1. `User.Role` refers to `Session.User.Role`. This leads to ([Cartesian product](https://en.wikipedia.org/wiki/Cartesian_product)) of the datasets and gives all the combination of `Session`<->`Roles` for the all active session.
1. `Name = "Administrator"` is the second filtering. The closest context for `Name` is `Role`. Equal `=` sign turns this part into testing dataset in the same way as `Active` did earlier.
1. Curly brackets `{}` means 'map'. In this case they declare the final resulting dataset, which will have two columns: a user name and session start time.

## Data Processing Flow
The previous example was rather a simple report. Let's consider a more sophisticated example.

	TBD
	
# Workflow
Workflow is a set of *States* and, *Actions*. 

## States
*State* is an conditional Aspect. Consider the following:

	PurchaseOrder is ToBeApproved when (Order->Amount > 100_000)
	
	ToBeApproved is State
	ToBeApproved has 
		ApproveAction (UserAction) when (User is Boss)
	User is Boss when (User->FullName = "John Smith")
	
	ToBeApproved has 
		ListForm
		MenuItem, Path "Main/Waiting for your approval"
	...

## Actions and third-party Events	
An Action or external Event is a persisted *Datom*. It will be stored into the database and must be processed by DataFlow: every such message can turn into an instance of an Object, turn its State or give some other kind of resulting **Data**. In previous paragraph's example we had an Action:

	ToBeApproved has 
		ApproveAction (UserAction) 

Now let's process it:

	ToBeApproved has 
		WasApproved when (this->ApproveAction) # means means that ApproveAction dataset not empty
		ApproveAction when (not WasApproved) # hide this Action when Approved
		
	PurchaseOrder has
		CanProceed (State) when (not ToBeApproved or WasApproved)

This is it. Now we can rely on the `CanProceed` State of the Order to process it further.

## State Machine
Let's design an old-style Workflow for a Ticket: *"New"->"Approved"->"Done"->"Closed"* with single Action on every State.

	Ticket has
		# New
		New (State)
		New has Approve (UserAction) whenNot (Approved)
		# Approved
		Approved (State)
			when (New->Approve)
		Approved has
			IDidIt (UserAction) whenNot (Done)
		# Done
		Done (State)
			when (Approved->IDidIt) 
		Done has 
			Close (UserAction) whenNot (Closed)
		# Closed
		Closed (State)
			when (Done->Close)

Cool. Now let's add `Deny` Action for the `New` State.

	Ticket has
		# Modify New State
		New has Deny (UserAction) whenNot (Denied)
		# Denied
		Denied (State) when (New->Deny)
		Denied has 
			Close (UserAction) whenNot (Closed)
		# Closed
		Closed when (Denied->Close)

The `Closed` state will now be active in two cases: if either `Done->Close` or `Denied->Close` has data.

Action can have additional fields. Consider:

	Ticket.New.Deny has
		Reason (LongText, Required)

From now on the Datom of `Deny` Action will require `Reason` in order to be saved.

## Switched states
Let's look at another example: a lamp switch:

	Lamp has
		On (State)
			when ([On]|[this->last] = On)
		On has
			TurnOff (UserAction)
		Off (State)
			when (this->last=Off)
		Off has
			TurnOn (UserAction)

You can declare arbitrary multi-state switch-like workfows in the same manner.

# Security and Consistency
## Access Control
Since all the changes in the system are *Datoms*, you only need to secure them.

There the following built-in Aspects to control the Access:
 * `Allowed` - the Aspect is unavailable and raises 'Access Denied' exceptyion when accessed.
 * `ReadOnly` - the Aspect is visible, but read-only.
 * `Queryable` - the Aspect is hidden from GUI and wildcarded queries, but is accesible (read-only) by direct request (ususally by Data Flows).

Consider:

	Document has 
		Approve (UserAction)
		Approve is Allowed 
			when (Session->User->IsBoss)
			
Now consider the the following examples of field-level Access Control.

	Document has
		InsertAction
		UpdateAction when (Session->User->Groups->Name='Maintainer'->exists)
		# Note that there is no DeleteAction here at all.
		
		Priority (EnumField)
		Priority has
			Minor (Option)
			Standard (Option)
			Major (Option) 
			Major is Allowed when (Session->User->Groups->Name='Maintainer'->exists) 
		Priority is ReadOnly 
			when (Priority=Major)
			whenNot (Session->User->Groups->Name='Maintainer'->exists)

## Validations
For the sake of data/logic consistency you may have *Validations* in your system. Every Validation should evaluate a required condition and generate error message when necessary. Consider:

	Document has
		Revision (AutoInc)
		RevisionValidation (Validation)
			Error "No more than three revisions are allowed for the Document."
			when (Document->Revision>3)
			
Every declared Validation in the system will be calculated before a `Datom` will get accessed by the Score Engine.

# Conflicts and Revoking
In a parallel world some Datoms may generate **conflicts** on the data level. The following Conflict-Resolution Strategy is being used in Score:
1. Delete always wins
1. Updates are being merged when possible.
1. The first arrived - wins.

TBD

# GUI declaration
There are the following built-in Aspect to declare your GUI:
* `ListForm` - the list of the Object with buttons representing allowed Actions.
* `EditForm` - the edit form with available Fields.
* `Section` and `Group` - the Aspects to organize your Fields. These Aspects are also respected by the Data Model to enforce Access Control. 
* `Menu` - the Aspect to declare Menu Items to access your interfaces.

There are also special Aspect to provide **Components** for the editors. There is nothing for therir implementation though - you shoud care about the concrete implementation for every Domain you use in your system on Gode Generation Stage. 

# Code Generation
Having you model described in SLing you will be provided with the compiled list of the Aspects by *Score Code Generation Engine* (**SCGE**). You are free to use any preferred code-generation libraries to generate platform-specific implementation for your system.

TBD

# Declaration Examples
```
Score has:
	# Bootstrapping
	Aspect is Aspect; 
	Context is Aspect; 
	Entity is Aspect; 
	Fragment is Aspect;
	Field is Aspect;
	List is Aspect; 
	Group is Aspect;
	Ref is Aspect; 
	#
	Context has Declarations (List): of Aspect;
	Entity has Fields (List): of Field; 
	List has Items(List);
	#
	# Application aspects
	Form is Aspect;
	Frame is Aspect;
	Menu is Aspect;
	#
	# Application aspects
	Action is Aspect;
	State is Aspect;
	#
	Storage is Aspect;

Application is Score


BroadView is Application
BroadView has:
	Document is Entity;
	Document has:
		ID;
		UserCreated;
		UserModified;
	ID is Autoinc;
	UserCreated is UserName: initValue="ctx->session->user->name", readonly;
	UserModified is UserName: value="ctx->session->user->name", alwaysPersist, readonly;
	UpdateDateTime is TimeStamp: value="ctx->timestamp", alwaysPersist, readonly;
	UserName is String: maxlen 40, validRE="[\w\d]{3,}";
	Caption for UserName: "User Name";
	Caption for UserCreated: "Created By";
	Caption for UserModified: "Modified By";
Core for BroadView;
#
Context for Assets:
	Asset is Entity;
	Asset is Asset.General;
	#
	Title (String): maxlen 100, hint "Title of the Asset"; 		### the field name will be equal to the caption
	#
	Asset.General has:
		Title (String): maxlen 100, hint "Title of the Asset"; 		### the field name will be equal to the caption
		EpisodeNo (String): maxlen 20, caption "Episode #", hint "Episode number within the Season"
	#
	Caption for Asset.General.EpisodeNo: "Episode #";
	Asset has Attachments;
	Asset has Notes; 
	#
	Program is Asset;
	Menu for Program: path "Programming", icon "program.png";
	Program has ProgramFields.General, ProgramFields.Additional, ProgramVersions;
	#
	ProgramFields.General is AssetFields.General
	#
	ProgramFields.General has
		"Episode #" (EpisodeNo) is a String: maxlen 20, hint "Episode number within the Season"		### the field name is provided in brackets
		"Episode Title" is a String: maxlen 100, hint "Episode number within the Season"		### the field name (EpisodeTitle) will be derived from the caption
		"Season" is a PositiveNumber: 
		"VChip" is a Reference to VChip
	#
	ProgramVersions is a Detail
	#			
	# 
	#
	Fields of Program has:
		General
		# 
		Title is a String: maxlen 100, hint "Title of the Asset" 		### the field name will be equal to the caption
```