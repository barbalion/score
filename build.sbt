name := "Score"

version := "0.1"

organization := "org.scorefw"

organizationName := "Score Framework Foundation"

organizationHomepage := Some(new URL("https://scorefw.org"))

scalaVersion := "2.13.1"

libraryDependencies ++= Seq(
  //"org.scalatest" %% "scalatest" % "3.1.0" % "test" withSources() withJavadoc()
  "org.antlr" % "antlr4" % "4.8",
  "com.github.scopt" %% "scopt" % "latest.release",
  "org.typelevel" %% "cats-core" % "latest.release"
)

//resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

isSnapshot := true

exportJars := true

enablePlugins(Antlr4Plugin)

antlr4Version in Antlr4 := "4.8"
antlr4PackageName in Antlr4 := Some("org.scorefw.dsl.gen")
antlr4GenListener in Antlr4 := true
antlr4GenVisitor in Antlr4 := true
javaSource in Antlr4 := baseDirectory.value / "gen"

lazy val root = project.in(file(".")).dependsOn(barbalion_lib)
lazy val barbalion_lib = RootProject(file("../barbalion-lib"))
